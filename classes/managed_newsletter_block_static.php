<?php

class managed_newsletter_block_static extends managed_newsletter_block
{
	public function build_html_content($account)
	{
		$objects = array(
		'global' => new stdClass(),
		'user' => $account
		);
		$result = token_replace_multiple($this->get_html(), $objects);
		return $result;
	}
	
	public function build_text_content($account)
	{
		$objects = array(
		'global' => new stdClass(),
		'user' => $account
		);
		
		$result = token_replace_multiple($this->get_text(), $objects);
		return $result;
	}
} 
