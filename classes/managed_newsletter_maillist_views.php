<?

class managed_newsletter_maillist_views extends managed_newsletter_maillist
{
	private $view;
	public function get_view()
	{
		return $this->view;
	}
	
	public function set_view($value)
	{
		$this->view = $value;
	}
	
	public function update($values)
	{
		parent::update($values);
		$this->set_view($values['view']);
	}
	
	public function save()
	{
		parent::save();
		db_query('DELETE FROM {managed_newsletters_maillists_settings} WHERE lid = %d', $this->get_lid());
		$sql = 'INSERT INTO {managed_newsletters_maillists_settings} (lsid, lid, name, value) VALUES (\'\', %d, \'%s\', \'%s\')';
		db_query($sql, $this->get_lid(), 'view', $this->get_view());
	}
	
	public function load()
	{
		parent::load();
		$result = db_query('SELECT * FROM {managed_newsletters_maillists_settings} WHERE lid = %d', $this->get_lid());
		while ($row = db_fetch_array($result))
		{
			$this->{$row['name']} = $row['value'];
		}
	}
	
	public function delete()
	{
		db_query('DELETE FROM {managed_newsletters_maillists_settings} WHERE lid = %d', $this->get_lid());
		parent::delete();
	}
	
	public function get_edit_form()
	{
		$form = parent::get_edit_form();
		$views = array();
		$result = db_query("SELECT v.vid, v.name FROM {view_view} v INNER JOIN {view_tablefield} f ON v.vid = f.vid INNER JOIN {view_tablefield} f2 ON v.vid = f2.vid WHERE f.tablename = 'usernode_users' AND f2.tablename = 'usernode_users' AND f.field = 'uid' AND f2.field = 'mail'");
		while($row = db_fetch_array($result))
		{
			$views[$row['vid']] = $row['name'];
		}
		$form['view'] = array(
		'#type' => 'select',
		'#title' => t('View'),
		'#description' => t('Select a view with list of users for the maillist. The view must contain 2 fields: user uid and mail. Create new view !link ', array('!link' => l(t('here'), 'admin/build/views'))),
		'#options' => $views,
		'#required' => true,
		'#default_value' => $this->get_view()
		);
		return $form;
	}
	
	function get_recipients()
	{
		$this->load();
		$view = views_get_view($this->get_view());
		$built_view = views_build_view('result', $view);
		$recipients = array();
		while($row = db_fetch_object($built_view['result']))
		{
			$recipients[$row->usernode_users_mail] = $row->usernode_users_uid;
		}
		return $recipients;
	}
}