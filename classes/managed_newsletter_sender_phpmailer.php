<?

require_once drupal_get_path('module', 'phpmailer') . '/includes/phpmailer.class.inc';
class managed_newsletter_sender_phpmailer extends managed_newsletter_sender
{
	private $phpmailer;

	public function __construct()
	{
		$this->phpmailer = new DrupalPHPMailer();
	}

	public function send_newsletter($newsletter, $maillists)
	{
		
		//TODO: move to parent class, make a method
		global $user;
		$lid = db_next_id('managed_newsletters_sending_log');
		$sql = 'INSERT INTO {managed_newsletters_sending_log} (lid, uid, tid, timestamp) VALUES (%d, %d, %d, %d)';
		db_query($sql, $lid, $user->uid, $newsletter->get_tid(), time());
		$newsletter->set_send_process_id($lid);
		$list = array();
		foreach($maillists as $maillist)
		{
			$list = $list + $maillist->get_recipients();
		}
		foreach($list as $mail => $uid)
		{
			$account = user_load(array('uid' => $uid));
			$newsletter->set_send_email_id(db_next_id('managed_newsletters_sent'));
			if ($this->send($newsletter, $account->uid, $mail, $newsletter->build_subject($account), $newsletter->build_html_content($account), $newsletter->build_text_content($account), $newsletter->build_from()))
			{
				drupal_set_message(t('@newsletter has been sent successfuly to @to', array('@newsletter' => $newsletter->get_title(), '@to' => $mail)));
			}
			else
			{
				drupal_set_message(t('@newsletter has not been sent to @to', array('@newsletter' => $newsletter->get_title(), '@to' => $mail)), 'error');
			}
			$newsletter->set_send_email_id(null);
		}
	}

	/**
	 *
	 */
	public function send_test($newsletter, $account)
	{
		$newsletter->set_send_email_id(db_next_id('managed_newsletters_sent'));
		$this->send($newsletter, $account->uid, $account->mail, $newsletter->build_subject($account) . ' (TEST)', $newsletter->build_html_content($account), $newsletter->build_text_content($account), $newsletter->build_from());
		$newsletter->set_send_email_id(null);
	}

	protected function send($newsletter, $uid, $to, $subject, $body, $body_text, $from = null, $headers = array())
	{

		// Fix empty From address. This resembles the behavior of Drupal 6 and later.
		if (!$from) {
			$from = variable_get('site_mail', ini_get('sendmail_from'));
		}

		// Parse 'From' e-mail address.
		$address        = phpmailer_parse_address($from);
		$this->phpmailer->From     = $address[0]['mail'];
		$this->phpmailer->FromName = $address[0]['name'];
		unset($headers['From']);

		// Extract CCs and BCCs from headers.
		if (isset($headers['CC'])) {
			foreach (phpmailer_parse_address($headers['CC']) as $address) {
				$this->phpmailer->AddCC($address['mail'], $address['name']);
			}
			unset($headers['CC']);
		}
		if (isset($headers['BCC'])) {
			foreach (phpmailer_parse_address($headers['BCC']) as $address) {
				$this->phpmailer->AddBCC($address['mail'], $address['name']);
			}
			unset($headers['BCC']);
		}

		// Extract Content-Type and charset.
		if (isset($headers['Content-Type'])) {
			$content_type = explode(';', $headers['Content-Type']);
			$this->phpmailer->ContentType = trim(array_shift($content_type));
			foreach ($content_type as $param) {
				$param = explode('=', $param, 2);
				$key = trim($param[0]);
				if ($key == 'charset') {
					$this->phpmailer->CharSet = trim($param[1]);
				}
				else {
					$this->phpmailer->ContentType .= '; ' . $key . '=' . trim($param[1]);
				}
			}
			unset($headers['Content-Type']);
		}

		// Set additional properties.
		$properties = array(
    	'X-Priority'                => 'Priority',
    	'Content-Transfer-Encoding' => 'Encoding',
    	'Sender'                    => 'Sender',
    	'Message-ID'                => 'MessageID',
		// Custom property.
		// @see DrupalPHPMailer::CreateHeader()
    	'Return-Path'               => 'ReturnPath',
		);
		foreach ($properties as $source => $property) {
			if (isset($headers[$source])) {
				$this->phpmailer->$property = $headers[$source];
				unset($headers[$source]);
			}
		}

		// This one is always set by PHPMailer.
		unset($headers['MIME-Version']);

		// Add remaining header lines.
		// Note: Any header lines MUST already be checked by the caller for unwanted
		// newline characters to avoid header injection.
		// @see PHPMailer::SecureHeader()
		foreach ($headers as $key => $value) {
			$this->phpmailer->AddCustomHeader("$key:$value");
		}

		// Check if debugging is turned on and replace recipient.
		if (variable_get('phpmailer_debug_email', '')) {
			$to = variable_get('phpmailer_debug_email', '');
		}

		// Set recipients.
		foreach (phpmailer_parse_address($to) as $address) {
			$this->phpmailer->AddAddress($address['mail'], $address['name']);
		}

		$this->phpmailer->Subject = $subject;
		
		// build in images
		preg_match_all ('/<img[^>]+src="(.*?)"[^>]*>/imS', $body, $matches);
		foreach($matches[1] as $match)
		{
			
			$cid = sha1($match);
			if ($this->phpmailer->AddEmbeddedImage($_SERVER['DOCUMENT_ROOT'].$match, $cid, $match))
			{
				$body = str_replace($match, 'cid:' . $cid, $body);
			}
		}
		
		$this->phpmailer->Body = $body;
		$this->phpmailer->AltBody = $body_text;

		if ($this->phpmailer->SMTPDebug) {
			ob_start();
		}

		if (!($result = $this->phpmailer->Send())) {
			watchdog('phpmailer', t('Error sending e-mail (from %from to %to): %error.', array('%from' => $from, '%to' => $to, '%error' => $this->phpmailer->ErrorInfo)), WATCHDOG_ERROR);
		}
		$this->log($newsletter, $uid, $to, '', $this->phpmailer->ErrorInfo);	
		// Reset object properties when keep-alive is enabled.
		//if ($this->phpmailer->SMTPKeepAlive) {
			$this->phpmailer->Reset();
		//}

		if ($this->phpmailer->SMTPDebug) {
			if ($debug = ob_get_contents()) {
				drupal_set_message($debug);
			}
			ob_end_clean();
		}
		
		return $result;

	}
}