<?php
class managed_newsletter_block_node extends managed_newsletter_block
{
	private $node_type;
	private $node_action;
	private $group_by;
	private $group_html;
	private $group_text;
	private $node_html;
	private $node_text;

	private $node_groups;

	public function set_node_types($value)
	{
		$this->node_type = array_filter($value);
	}

	public function set_node_actions($value)
	{
		$this->node_action = array_filter($value);
	}

	public function set_group_by($value)
	{
		$this->group_by = $value;
	}

	public function get_node_types()
	{
		return (array)$this->node_type;
	}

	public function get_node_actions()
	{
		return (array)$this->node_action;
	}

	public function get_group_by()
	{
		return $this->group_by;
	}

	public function load()
	{
		parent::load();
		$result = db_query('SELECT * FROM {managed_newsletters_block_node_settings} WHERE tid = %d', $this->get_tid());
		while ($row = db_fetch_array($result))
		{
			if ($row['name'] == 'node_action' || $row['name'] == 'node_type')
			{
				$this->{$row['name']}[] = $row['value'];
			}
			else
			{
				$this->{$row['name']} = $row['value'];
			}
		}
	}

	public function get_edit_form()
	{
		$form = parent::get_edit_form();
		$form['node_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Node types'),
		'#options' => node_get_types('names'),
		'#default_value' => (count($this->get_node_types()) > 0 ) ? array_combine($this->get_node_types(), $this->get_node_types()) : array(),
		'#description' => t('Select types of nodes which can be added in the block')
		);

		$form['node_actions'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Node actions'),
		'#options' => array('insert' => 'insert', 'update' => 'update'),
		'#default_value' => (count($this->get_node_actions()) > 0 ) ? array_combine($this->get_node_actions(), $this->get_node_actions()) : array(),
		'#description' => t('Select types of nodes which can be added in the block')
		);


		$group_by = array('' => t('None'));
		foreach (taxonomy_get_vocabularies() as $voc)
		{
			$group_by['vocabulary_' . $voc->vid] = $voc->name;
		}

		$form['group_by'] = array(
		'#type' => 'select',
		'#title' => t('Group By'),
		'#options' => $group_by,
		'#default_value' => $this->get_group_by(),
		'#description' => t('')
		);

		$form['templates'] = array(
		'#type' => 'fieldset',
		'#title' => t('Templates')
		);

		$form['templates']['template_group'] = array(
		'#type' => 'fieldset',
		'#title' => t('Group'),
		'#tree' => true
		);
		$form['templates']['template_group']['html'] = array(
		'#type' => 'textarea',
		'#title' => t('Html template'),
		'#default_value' => $this->group_html,
		'#required' => true,
		'#description' => t('Used for generation of html part of newsletters')
		);
		$form['templates']['template_group']['text'] = array(
		'#type' => 'textarea',
		'#title' => t('Text template'),
		'#required' => true,
		'#default_value' => $this->group_text,
		'#description' => t('Used for generation of plain text part of newsletters')
		);

		$form['templates']['template_group']['tokens'] = array(
		'#type' => 'fieldset',
		'#title' => t('Available tokens'),
		'#weight' => 9,
		'#collapsible' => true,
		'#collapsed' => true
		);
		$form['templates']['template_group']['tokens']['tokens'] = array(
		'#type' => 'markup',
		'#value' => $this->get_tokens_table($this->get_tokens('group'))
		);

		$form['templates']['template_node'] = array(
		'#type' => 'fieldset',
		'#title' => t('Node'),
		'#tree' => true
		);
		$form['templates']['template_node']['html'] = array(
		'#type' => 'textarea',
		'#title' => t('Html template'),
		'#default_value' => $this->node_html,
		'#required' => true,
		'#description' => t('Used for generation of html part of newsletters')
		);
		$form['templates']['template_node']['text'] = array(
		'#type' => 'textarea',
		'#title' => t('Text template'),
		'#required' => true,
		'#default_value' => $this->node_text,
		'#description' => t('Used for generation of plain text part of newsletters')
		);
		$form['templates']['template_node']['tokens'] = array(
		'#type' => 'fieldset',
		'#title' => t('Available tokens'),
		'#weight' => 9,
		'#collapsible' => true,
		'#collapsed' => true
		);
		$form['templates']['template_node']['tokens']['tokens'] = array(
		'#type' => 'markup',
		'#value' => $this->get_tokens_table($this->get_tokens('node'))
		);

		return $form;
	}

	public function validate_values($values)
	{
		parent::validate_values($values);
	}

	public function update($values)
	{
		parent::update($values);
		$this->set_node_types($values['node_types']);
		$this->set_node_actions($values['node_actions']);
		$this->set_group_by($values['group_by']);
		$this->group_html = $values['template_group']['html'];
		$this->group_text = $values['template_group']['text'];
		$this->node_html = $values['template_node']['html'];
		$this->node_text = $values['template_node']['text'];
	}

	public function save()
	{
		parent::save();
		db_query('DELETE FROM {managed_newsletters_block_node_settings} WHERE tid = %d', $this->get_tid());
		$sql = 'INSERT INTO {managed_newsletters_block_node_settings} (tsid, tid, name, value) VALUES (\'\', %d, \'%s\', \'%s\')';
		foreach($this->get_node_types() as $type)
		{
			db_query($sql, $this->get_tid(), 'node_type', $type);
		}

		foreach($this->get_node_actions() as $action)
		{
			db_query($sql, $this->get_tid(), 'node_action', $action);
		}

		db_query($sql, $this->get_tid(), 'group_by', $this->get_group_by());
		db_query($sql, $this->get_tid(), 'group_html', $this->group_html);
		db_query($sql, $this->get_tid(), 'group_text', $this->group_text);
		db_query($sql, $this->get_tid(), 'node_html', $this->node_html);
		db_query($sql, $this->get_tid(), 'node_text', $this->node_text);

	}

	public function delete()
	{
		db_query('DELETE FROM {managed_newsletters_block_node_settings} WHERE tid = %d', $this->get_tid());
		db_query('DELETE FROM {managed_newsletters_block_node_queue} WHERE tid = %d', $this->get_tid());
		parent::delete();
	}

	public static function add_in_queue($tid, $nid)
	{
		if (!managed_newsletter_block_node::is_in_queue($tid, $nid))
		{
			$add_sql = "INSERT INTO {managed_newsletters_block_node_queue} (tqid, tid, nid, added, sorting) VALUES ('', %d, %d, %d, 0)";
			db_query($add_sql, $tid, $nid, time());
		}
	}

	public static function delete_from_queue($tid, $nid)
	{
		db_query('DELETE FROM {managed_newsletters_block_node_queue} WHERE tid = %d AND nid = %d', $tid, $nid);
	}

	public static function get_queues($type, $op = null)
	{
		if ($op)
		{
			$sql = "SELECT t1.tid FROM {managed_newsletters_block_node_settings} t1 INNER JOIN {managed_newsletters_block_node_settings} t2 ON t1.tid=t2.tid WHERE t1.name='node_type' AND t1.value='%s' AND t2.name='node_action' AND t2.value='%s'";
		}
		else
		{
			$sql = "SELECT t1.tid FROM {managed_newsletters_block_node_settings} t1 WHERE t1.name='node_type' AND t1.value='%s'";
		}
		$result = db_query($sql, $type, $op);
		$tids = array();
		while($row = db_fetch_array($result))
		{
			$tids[] = $row['tid'];
		}
		return $tids;
	}

	public static function is_in_queue($tid, $nid)
	{
		return db_result(db_query('SELECT count(*) as c FROM {managed_newsletters_block_node_queue} WHERE tid = %d AND nid = %d', $tid, $nid));
	}

	public function get_tokens($field = null)
	{
		$tokens = parent::get_tokens();
		$tokens += $this->get_class_tokens();
		return $tokens;
	}

	public function get_class_tokens($field = null)
	{
		$class = get_class($this);
		$tokens[$class]['managed-newsletter']['managed-newsletter-node-block-content'] = 'Content of node block (set of nodes or groups of nodes)';
		switch ($field)
		{
			case 'group':
				$tokens[$class]['managed-newsletter']['managed-newsletter-group-content'] = 'Set of nodes in the group';
				$tokens[$class]['managed-newsletter']['managed-newsletter-group-title'] = 'Title of the group (term name, etc.)';
				break;
			case 'node':
				$tokens['node'] = token_get_list('node');
				break;
		}
		return $tokens;
	}

	public function build_html_content($account)
	{
		$objects = array(
		'global' => new stdClass(),
		'user' => $account,
		);
		$result = token_replace_multiple($this->build_inner_content(1), $objects);
		return $result;
	}

	public function build_text_content($account)
	{
		$objects = array(
		'global' => new stdClass(),
		'user' => $account, 
		);
		$result = token_replace_multiple($this->build_inner_content(0), $objects);
		return $result;
	}

	private function build_inner_content($html = 1)
	{
		$output = '';
		$groups = array();
		foreach($this->get_nodes() as $group_title => $nodes)
		{
			$values['managed-newsletter-group-title'] = $group_title;

			$values['managed-newsletter-group-content'] = '';
			foreach($nodes as $node)
			{
				$objects['node'] = $node;
				$template = $html ? $this->node_html : $this->node_text;
				$values['managed-newsletter-group-content'] .=  token_replace_multiple($template, $objects);
			}
			$group_template = $html ? $this->group_html : $this->group_text;
			$output .= _token_replace_tokens($group_template, array_keys($values), array_values($values), '[', ']');
		}
		$block_template = $html ? $this->get_html() : $this->get_text();
		$values = array();
		$values['managed-newsletter-node-block-content'] = $output;
		$output = _token_replace_tokens($block_template, array_keys($values), array_values($values), '[', ']');
		return $output;
	}

	public function get_nodes()
	{
		if (!isset($this->node_groups))
		{
			$this->node_groups = array();
			$sql = "SELECT * FROM {managed_newsletters_block_node_queue} WHERE tid = %d ORDER BY sorting ASC, added DESC";
			$result = db_query($sql, $this->get_tid());
			$group_by = explode('_', $this->get_group_by());
			while ($row = db_fetch_array($result))
			{
				$node = node_load($row['nid']);
				if ($node->nid)
				{
					if ($group_by[0] == 'vocabulary' && $group_by[1])
					{
						foreach($node->taxonomy as $tid => $term)
						{
							if ($term->vid == $group_by[1])
							{
								$this->node_groups[$term->name][$node->nid] = $node;
								break;
							}
						}
					}
					else
					{
						$this->node_groups[0][$node->nid] = $node;
					}
				}
			}
		}
		return $this->node_groups;
	}

	public function load_changed_nodes($values)
	{
		$this->node_groups = array();
		if (is_array($values))
		{
			uasort($values, array("managed_newsletter_block_node", "sort_by_order"));
			foreach($values as $name => $group)
			{
				if (count($group['nodes']) > 0)
				{
					$this->node_groups[$group['name']] = array();
					uasort($group['nodes'], array("managed_newsletter_block_node", "sort_by_order"));
					foreach($group['nodes'] as $nid => $n)
					{
						$node = node_load($nid);
						if ($node->nid)
						{
							$node->title = preg_replace("#(<\/?)(\w+)([^>]*>)#e", "", $n['title']);
							$node->teaser = preg_replace("#(<\/?)(\w+)([^>]*>)#e", "", $n['teaser']);
							$this->node_groups[$group['name']][$node->nid] = $node;
						}
					}
				}

			}
		}
	}
	public static function sort_by_order($a, $b)
	{
		$al = intval($a['order']);
		$bl = intval($b['order']);

		if ($al == $bl)
		{
			return 0;
		}
		return ($al > $bl) ? +1 : -1;
	}

	public function reset_queue($values)
	{
		$nids = array(0);
		foreach($values as $node => $action)
		{
			if ($action == 'hold')
			{
				$nids[] = $node;
			}
		}
		db_query('DELETE FROM {managed_newsletters_block_node_queue} WHERE tid = %d AND nid NOT IN (' . implode(', ', $nids) . ')', $this->get_tid());
	}

}

