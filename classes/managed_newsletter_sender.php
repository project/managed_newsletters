<?

abstract class managed_newsletter_sender
{
	public static function set_deafult_sender($sender)
	{
		variable_set('managed_newsletters_default_sender', $sender);
	}

	public static function get_deafult_sender()
	{
		return variable_get('managed_newsletters_default_sender', 'managed_newsletter_sender_phpmailer');
	}
	
	public static function load_deafult_sender()
	{
		$default_sender = managed_newsletter_sender::get_deafult_sender();
		if (class_exists($default_sender))
		{
			return new $default_sender();
		}
	}
	
	protected function log($newsletter, $uid, $mail, $content, $error = null)
	{
		$sql = "INSERT INTO {managed_newsletters_sent} (sid, uid, lid, mail, content, error, timestamp) VALUES (%d, %d, %d, '%s', '%s', '%s', %d)";
		db_query($sql, $newsletter->get_send_email_id(), $uid, $newsletter->get_send_process_id(), $mail, $content, $error, time());
	}
	
	public abstract function send_newsletter($newsletter, $maillists);
	public abstract function send_test($newsletter, $account);
} 