<?

abstract class managed_newsletter_maillist
{
	protected $lid;
	protected $title;
	
	public function get_lid()
	{
		return $this->lid;
	}
	
	public function get_title()
	{
		return $this->title;
	}
	
	public function set_title($value)
	{
		$this->title = $value;
	}	
	
	public function load()
	{
		
	}
	
	public function update($values)
	{
		$this->set_title($values['title']);
	}
	
	public function save()
	{
		if ($this->get_lid())
		{
			$sql = "UPDATE {managed_newsletters_maillists} SET title='%s' WHERE lid = %d";
			db_query($sql, $this->get_title(), $this->get_lid());
		}
		else
		{
			
			$this->lid = db_next_id('managed_newsletters_maillists');
			$sql = "INSERT INTO {managed_newsletters_maillists} (lid, class, title) VALUES (%d, '%s', '%s')";
			db_query($sql, $this->get_lid(), get_class($this), $this->get_title());
		}
		cache_clear_all();
	}
	
	public function delete()
	{
		db_query('DELETE FROM {managed_newsletters_maillists} WHERE lid = %d', $this->get_lid());
		cache_clear_all();
	}
	
	public static function get_all($lid = null, $class = null)
	{
		$sql = 'SELECT * FROM {managed_newsletters_maillists}';
		$conditions = array();
		
		$where = array();
		if ($class)
		{
			$where[] =  ' class = \'%s\'';
			$conditions[] = $class;
		}
		
		if ($lid)
		{
			$where[] =  ' lid = \'%d\'';
			$conditions[] = $lid;
		}
		
		if (!empty($where))
		{
			$sql .= ' WHERE ' . implode(' AND ', $where);
		}
		$result = db_query($sql, $conditions);
		$return = array();
		while($row = db_fetch_array($result))
		{
			if (class_exists($row['class']))
			{
				$return[$row['lid']] = new $row['class']();
				unset($row['class']);
				foreach($row as $key => $value)
				{
					$return[$row['lid']]->$key = $value;	
				}
			}
			else
			{
				// throw exception
			}
		}
		return $lid ? $return[$lid] : $return;
	}
	public function get_edit_form()
	{
		$this->load();
		$form = array();
		
		$form['maillist'] = array(
		'#type' => 'value',
		'#value' => $this
		);
		
		$form['title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#default_value' => $this->title,
		'#required' => true,
		'#description' => t('Used only for presentation')
		);
		
		$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 10
		);
		return $form;
	}
	
	abstract function get_recipients();
} 