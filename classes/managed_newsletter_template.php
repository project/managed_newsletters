<?php

abstract class managed_newsletter_template{
	protected $tid;
	protected $name;
	protected $title;
	protected $html;
	protected $text;

	public function __construct($tid = null)
	{
		if ($tid)
		{
			$this->tid = $tid;
			$this->load();
		}	
	}
	
	public function get_tid()
	{
		return $this->tid;
	}
	
	public function get_name()
	{
		return $this->name;
	}
	
	public function get_title()
	{
		return $this->title;
	}
	
	public function get_html()
	{
		return $this->html;
	}
	
	public function get_text()
	{
		return $this->text;
	}

	
	public function set_name($value)
	{
		$this->name = $value;
	}
	
	public function set_title($value)
	{
		$this->title = $value;
	}
	
	public function set_html($value)
	{
		$this->html = $value;
	}
	
	public function set_text($value)
	{
		$this->text = $value;
	}
	
	public function get_edit_form()
	{
		$this->load();
		$form = array();
		$form['template'] = array(
		'#type' => 'value',
		'#value' => $this
		);
		$form['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Name'),
		'#required' => true,
		'#default_value' => $this->name,
		'#description' => t('Machine-readable name of template, used as name of a token, must be unique')
		);
		$form['title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#default_value' => $this->title,
		'#required' => true,
		'#description' => t('Template title, used only for presentation')
		);
		$form['main_template'] = array(
		'#type' => 'fieldset',
		'#title' => t('Templates')
		);
		$form['main_template']['html'] = array(
		'#type' => 'textarea',
		'#title' => t('Html template'),
		'#default_value' => $this->html,
		'#required' => true,
		'#description' => t('Html template, used for generation of html part of newsletters')
		);
		
		$form['main_template']['text'] = array(
		'#type' => 'textarea',
		'#title' => t('Text template'),
		'#required' => true,
		'#default_value' => $this->text,
		'#description' => t('Text template, used for generation of plain text part of newsletters')
		);
		
		$form['main_template']['tokens'] = array(
		'#type' => 'fieldset',
		'#title' => t('Available tokens'),
		'#collapsible' => true,
		'#collapsed' => true
		);
		$form['main_template']['tokens']['tokens'] = array(
		'#type' => 'markup',
		'#value' => $this->get_tokens_table()
		);
		$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#weight' => 10
		);
		return $form;
	}
	
	
	public static function get_all($class = null, $tid = null,  $name = null, $isblock = null)
	{
		$conditions = array();
		$sql = 'SELECT tid, class, name, title FROM {managed_newsletters_templates}';
		$where = array();
		if ($class)
		{
			$where[] =  ' class = \'%s\'';
			$conditions[] = $class;
		}
		
		if ($tid)
		{
			$where[] =  ' tid = \'%d\'';
			$conditions[] = $tid;
		}
		
		if ($name)
		{
			$where[] =  ' name = \'%s\'';
			$conditions[] = $name;
		}
		
		if ($isblock !== null)
		{
			$where[] =  ' block = \'%s\'';
			$conditions[] = (int)$isblock;
		}
		
		if (!empty($where))
		{
			$sql .= ' WHERE ' . implode(' AND ', $where);
		}
		$result = db_query($sql, $conditions);
		$return = array();
		while($row = db_fetch_array($result))
		{
			if (class_exists($row['class']))
			{
				$return[$row['tid']] = new $row['class']();
				unset($row['class']);
				foreach($row as $key => $value)
				{
					$return[$row['tid']]->$key = $value;	
				}
			}
			else
			{
				// throw exception
			}
		}
		return isset($tid) ? (isset($return[$tid]) ? $return[$tid] : false) : $return;
	}
	
	public function load()
	{
		$sql = 'SELECT * FROM {managed_newsletters_templates} WHERE tid = %d';
		$values = db_fetch_array(db_query($sql, $this->tid));
		foreach((array)$values as $key => $value)
		{
			$this->$key = $value;	
		}
	}
	
	public function validate_values($values)
	{
		// check that the name is unique
		$templates = managed_newsletter_template::load(null, null, $values['name']);
		if (count($templates) == 1 && !isset($templates[$this->get_tid()]))
		{
			form_set_error('name', t('Template name is already in use'));
		}
	}
	
	public function update($values)
	{
		$this->set_name($values['name']);
		$this->set_title($values['title']);
		$this->set_text($values['text']);
		$this->set_html($values['html']);
	}
	
	public function save()
	{
		if ($this->tid)
		{
			$sql = "UPDATE {managed_newsletters_templates} SET name='%s', title='%s', html='%s', text='%s' WHERE tid = %d";
			db_query($sql, $this->name, $this->title, $this->html, $this->text, $this->tid);
		}
		else
		{
			
			$this->tid = db_next_id('managed_newsletters_templates');
			$sql = "INSERT INTO {managed_newsletters_templates} (tid, block, class, name, title, html, text) VALUES (%d, %d, '%s', '%s', '%s', '%s', '%s')";
			db_query($sql, $this->tid, (int)is_a($this, 'managed_newsletter_block'), get_class($this), $this->name, $this->title, $this->html, $this->text);
		}
		cache_clear_all();
	}
	
	public function delete()
	{
		db_query('DELETE FROM {managed_newsletters_template_blocks} WHERE tid = %d OR tbid = %d', $this->get_tid(), $this->get_tid());
		db_query('DELETE FROM {managed_newsletters_templates} WHERE tid = %d', $this->get_tid());
		cache_clear_all();
	}
	
	
	public function get_tokens()
	{
		$tokens = array();
		$tokens['global'] = token_get_list('global');
		$tokens['user']= token_get_list('user');
		$tokens['managed_newsletter']= token_get_list('managed_newsletter');
		return $tokens;
	}
	
	
	public function get_tokens_table($tokens_list = null)
	{
		$tokens_list = $tokens_list == null ? $this->get_tokens() : $tokens_list; 
		$header = array(t('Token'), t('Descriptions'));
		$rows = array();
		foreach($tokens_list as $type => $tokens_set)
		{
			foreach($tokens_set as $type => $tokens)
			{
				$rows[] = array(array('data' => $type, 'colspan' => 2, 'style' => 'text-align:center;font-weight:bold;'));
				foreach($tokens as $token => $description)
				{
					$rows[] = array('[' . $token . ']', $description);
				}
			}
		}
		return theme('table', $header, $rows);
	}
	
	public abstract function build_html_content($account);
	public abstract function build_text_content($account);
}
