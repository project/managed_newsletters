<?php

class managed_newsletter_newsletter extends managed_newsletter_template
{
	private $blocks = array();
	private $blocks_templates;
	private $subject;
	private $default_maillists;
	private $from;
	private $lid;
	private $send_email_id;

	private $blocks_content = array();

	public function get_subject()
	{
		return $this->subject;
	}

	public function set_subject($value)
	{
		$this->subject = $value;
	}

	public function get_from()
	{
		return $this->from;
	}

	public function set_from($value)
	{
		$this->from = $value;
	}

	public function get_default_maillists()
	{
		return unserialize($this->default_maillists);
	}

	public function set_default_maillists($value)
	{
		$this->default_maillists = serialize(array_filter($value));
	}

	public function get_blocks()
	{
		return (array) $this->blocks;
	}

	public function set_blocks($value)
	{
		$this->blocks = array_filter($value);
	}

	public function get_edit_form()
	{
		$form = parent::get_edit_form();
		$form['subject'] = array(
		'#type' => 'textfield',
		'#title' => t('Subject'),
		'#default_value' => $this->get_subject(),
		'#description' => t('Email subject. Global and user tokens are available')
		);

		$form['from'] = array(
		'#type' => 'textfield',
		'#title' => t('From'),
		'#default_value' => $this->get_from(),
		'#description' => t('From field for email. Global tokens are available')
		);

		$maillist = array();
		foreach(managed_newsletter_maillist::get_all() as $item)
		{
			$maillist[$item->get_lid()] = $item->get_title() . ' ' . l('Preview', 'admin/managed_newsletters/maillists/preview/' . $item->get_lid(), array('target' => '_blank'));
		}

		$form['default_maillists'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Default Mail List'),
		'#options' => $maillist,
		'#default_value' => $this->get_default_maillists(),
		'#description' => t('Mail lists for quick send method')
		);

		$blocks = array();
		foreach(managed_newsletter_template::get_all(null, null, null, 1) as $block_template)
		{
			$blocks[$block_template->get_tid()] = $block_template->get_title();
		}

		$form['blocks'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Enabled blocks'),
		'#options' => $blocks,
		'#default_value' => (count($this->get_blocks()) > 0 ) ? array_combine($this->get_blocks(), $this->get_blocks()) : array(),
		'#description' => t('Select blocks you want to use in this newsletter')
		);

		return $form;
	}

	public function load()
	{
		parent::load();
		$result = db_query('SELECT * FROM {managed_newsletters_template_blocks} WHERE tid = %d', $this->get_tid());
		$blocks = array();
		while ($row = db_fetch_array($result))
		{
			$blocks[] = $row['bid'];
		}
		$this->set_blocks($blocks);

		$result = db_query('SELECT * FROM {managed_newsletters_block_node_settings} WHERE tid = %d', $this->get_tid());
		while ($row = db_fetch_array($result))
		{
			$this->{$row['name']} = $row['value'];
		}
	}

	public function update($values)
	{
		parent::update($values);
		$this->set_blocks($values['blocks']);
		$this->set_subject($values['subject']);
		$this->set_from($values['from']);
		$this->set_default_maillists($values['default_maillists']);
	}

	public function save()
	{
		parent::save();
		db_query('DELETE FROM {managed_newsletters_template_blocks} WHERE tid = %d', $this->get_tid());
		$sql = 'INSERT INTO {managed_newsletters_template_blocks} (tbid, tid, bid) VALUES (\'\', %d, %d)';
		foreach($this->get_blocks() as $block)
		{
			db_query($sql, $this->get_tid(), $block);
		}

		db_query('DELETE FROM {managed_newsletters_block_node_settings} WHERE tid = %d', $this->get_tid());
		$sql = 'INSERT INTO {managed_newsletters_block_node_settings} (tsid, tid, name, value) VALUES (\'\', %d, \'%s\', \'%s\')';
		db_query($sql, $this->get_tid(), 'subject', $this->get_subject());
		db_query($sql, $this->get_tid(), 'from', $this->get_from());
		db_query($sql, $this->get_tid(), 'default_maillists', $this->default_maillists);
	}

	public function &get_blocks_templates($block_tid = null)
	{
		if (!isset($this->blocks_templates))
		{
			$this->blocks_templates = array();
			foreach($this->get_blocks() as $tid)
			{
				$block = managed_newsletter_template::get_all(null, $tid);
				$block->load();
				$this->blocks_templates[$block->get_tid()] = $block;
			}
		}
		
		return ($block_tid == null ? $this->blocks_templates : $this->blocks_templates[$block_tid]);
	}

	public function get_tokens()
	{
		$tokens = array();//parent::get_tokens();
		$tokens += $this->get_class_tokens();
		return $tokens;
	}

	public function get_class_tokens()
	{
		$class = get_class($this);
		$tokens[$class] = array();
		foreach(managed_newsletter_template::get_all(null, null, null, 1) as $tid => $block)
		{
			$tokens[$class]['newsletter_blocks']['managed-newsletter-block-' . $block->get_name()] = $block->get_title();
		}
		return $tokens;
	}

	public function build_html_content($account)
	{
		$values = array();
		foreach($this->get_blocks_templates() as $tid => $block)
		{
			$values['managed-newsletter-block-' . $block->get_name()] = $block->build_html_content($account);
		}
		$result = _token_replace_tokens($this->get_html(), array_keys($values), array_values($values), '[', ']');
		
		$objects = array(
		'global' => null,
		'user' => $account,
		'managed_newsletter' => $this
		);
		$result = token_replace_multiple($result, $objects);		
		return $result;
	}

	public function build_text_content($account)
	{
		$values = array();
		foreach($this->get_blocks_templates() as $tid => $block)
		{
			$values['managed-newsletter-block-' . $block->get_name()] = $block->build_text_content($account);
		}
		$result = _token_replace_tokens($this->get_text(), array_keys($values), array_values($values), '[', ']');
		
		$objects = array(
		'global' => null,
		'user' => $account,
		'managed_newsletter' => $this
		);
		$result = token_replace_multiple($result, $objects);		
		return $result;
	}

	public function build_subject(&$account)
	{
		$objects = array(
		'global' => null,
		'user' => $account,
		'managed_newsletter' => $this
		);
		$result = token_replace_multiple($this->get_subject(), $objects);
		return $result;
	}

	public function build_from()
	{
		$objects = array(
		'global' => null,
		);
		$result = token_replace_multiple($this->get_from(), $objects);
		return $result;
	}
	
	public function set_send_process_id($value)
	{
		$this->lid = $value;
	}
	
	public function get_send_process_id()
	{
		return $this->lid;
	}
	
	public function set_send_email_id($value)
	{
		$this->send_email_id = $value;
	}
	
	public function get_send_email_id()
	{
		return $this->send_email_id;
	}	
}
