<?php

function managed_newsletters_send_form($mode, &$form_values = null)
{
	$form = array();
	// create multistep form
	$form['#multistep'] = TRUE;
	$form['#redirect'] = FALSE;

	if (!isset($form_values))
	{
		unset($_SESSION['managed_newsletters_send']);
		if ($mode == 'full')
		{
			$step = 1;
		}
		else
		{
			$n = managed_newsletter_template::get_all('managed_newsletter_newsletter', $mode);
			if ($n)
			{
				$form_values['newsletter'] = $mode;
				$_SESSION['managed_newsletters_send']['newsletter'] = $mode;
				$step = 2;
			}
			else
			{
				$step = 1;
			}
		}
	}
	else
	{
		if ($form_values['op'] == t('Back'))
		{
			$step = $form_values['step'] - 2;

			if ($mode == 'full' && $step <= 1)
			{
				$back = false;
			}
			elseif ($mode != 'full' && $step <= 2)
			{
				$back = false;
			}
			else
			{
				$back = true;
			}
		}
		else
		{
			$step = $form_values['step'];
			$back = true;
		}

	}

	$form['step'] = array(
	'#type' => 'hidden',
	'#value' => $step + 1
	);


	switch ($step)
	{
		case 1:
			managed_newsletters_send_form_step_newsletter($form, $form_values);
			break;
		case 2:
			managed_newsletters_send_form_step_queue($form, $form_values);
			break;
		case 3:
			managed_newsletters_send_form_step_order($form, $form_values);
			break;
		case 4:
			managed_newsletters_send_form_step_edit($form, $form_values);
			break;
		case 5:
			managed_newsletters_send_form_step_test($form, $form_values);
			break;
		case 6:
			managed_newsletters_send_form_step_send($form, $form_values);
			break;
		case 7:
			managed_newsletters_send_form_step_reset_queue($form, $form_values);
			$back = false;
			break;

	}
	if ($back)
	{
		$form['back'] = array(
		'#type' => 'submit',
		'#value' => t('Back'),
		'#weight' => 9
		);
	}
	$form['cancel'] = array(
	'#type' => 'submit',
	'#value' => t('Cancel'),
	'#weight' => 15,
	'#attributes' => array('style' => 'margin-left:5em;')
	);
	return $form;
}

function managed_newsletters_send_form_submit($form_id, &$form_values)
{
	if ($form_values['op'] == t('Cancel'))
	{
		unset($_SESSION['managed_newsletters_send']);
		drupal_goto($_GET['q']);
	}

	if (!isset($_SESSION['managed_newsletters_send']))
	{
		$_SESSION['managed_newsletters_send'] = array();
	}
	$_SESSION['managed_newsletters_send'] = $form_values + $_SESSION['managed_newsletters_send'];

	if ($form_values['op'] == t('Back'))
	{
		return;
	}

	switch ($form_values['step'])
	{
		case 1:
			break;
		case 2:
			break;
		case 6:
			managed_newsletters_send_test();
			break;
		case 7:
			managed_newsletters_send();
			break;
		case 8:
			managed_newsletters_reset_queue();
			unset($_SESSION['managed_newsletters_send']);
			drupal_goto($_GET['q']);
			break;

	}
}

function managed_newsletters_send_form_step_newsletter(&$form, &$form_values)
{
	$newsletters = array();
	foreach(managed_newsletter_template::get_all(null, null, null, 0) as $item)
	{
		$newsletters[$item->get_tid()] = $item->get_title();
	}

	$maillist = array();
	foreach(managed_newsletter_maillist::get_all() as $item)
	{
		$maillist[$item->get_lid()] = $item->get_title() . ' ' . l('Preview', 'admin/managed_newsletters/maillists/preview/' . $item->get_lid(), array('target' => '_blank'));
	}

	$form['newsletter'] = array(
	'#type' => 'radios',
	'#title' => t('Newsletter'),
	'#options' => $newsletters,
	'#default_value' => $_SESSION['managed_newsletters_send']['newsletter'],
	'#required' => true,
	'#description' => t('Select newsletter you want to send')
	);
	$form['maillists'] = array(
	'#type' => 'checkboxes',
	'#title' => t('Mail lists'),
	'#options' => $maillist,
	'#default_value' => (array)$_SESSION['managed_newsletters_send']['maillists'],
	'#required' => true,
	'#description' => t('Select mail lists you want to send the newsletter to')
	);
	$form['next'] = array(
	'#type' => 'submit',
	'#value' => t('Next'),
	'#weight' => 10
	);
}


function managed_newsletters_send_form_step_queue(&$form, &$form_values)
{
	if ($newsletter = managed_newsletter_template::get_all(null, $_SESSION['managed_newsletters_send']['newsletter']))
	{
		$newsletter->load();
		$form['queue'] = array(
		'#tree' => true
		);
		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$form['queue'][$tid] = array(
			'#type' => 'fieldset',
			'#title' => $block->get_title(),
			'#tree' => true
				);
				foreach($block->get_nodes() as $group)
				{
					foreach($group as $node)
					{
						$form['queue'][$tid][$node->nid] = array(
					'#type' => 'radios',
					'#options' => array('use' => t('Use'), 'hold' => t('Hold'), 'remove' => t('Discard')),
					'#title' => $node->title,
					'#default_value' => isset($_SESSION['managed_newsletters_send']['queue'][$tid][$node->nid]) ? $_SESSION['managed_newsletters_send']['queue'][$tid][$node->nid] : 'use' 
					);
					}
				}
			}
		}
		$form['next'] = array(
		'#type' => 'submit',
		'#value' => t('Next'),
		'#weight' => 10
		);
	}
	else
	{
		drupal_set_message(t('Selected newsletter has not been found'), 'error');
	}
}

function managed_newsletters_send_form_step_order(&$form, &$form_values)
{
	if ($newsletter = managed_newsletter_template::get_all(null, $_SESSION['managed_newsletters_send']['newsletter']))
	{
		$newsletter->load();
		/*@var $newsletter managed_newsletter_newsletter*/

		$form['order'] = array(
		'#tree' => true
		);
		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$form['order'][$tid] = array(
			'#type' => 'fieldset',
			'#title' => $block->get_title(),
			'#tree' => true
				);
				$groups = array();
				foreach($block->get_nodes() as $group => $nodes)
				{
					$groups[form_clean_id($group)] = $group;
				}
				$gr_ord = 1;
				foreach($block->get_nodes() as $group => $nodes)
				{
					$form['order'][$tid][form_clean_id($group)]['#prefix'] = '<div class="managed-newsletters-group"><div class="managed-newsletters-group-buttons"><img class="managed-newsletters-group-button-up" src="/profiles/default/modules/gcl_search_saved/images/up.png"><img class="managed-newsletters-group-button-down" src="/profiles/default/modules/gcl_search_saved/images/down.png"></div>';
					$form['order'][$tid][form_clean_id($group)]['#type'] = 'fieldset';
					$form['order'][$tid][form_clean_id($group)]['#title'] = $group;
					$form['order'][$tid][form_clean_id($group)]['#attributes'] = array('id' => form_clean_id($group));
					$form['order'][$tid][form_clean_id($group)]['#tree'] = true;
					$form['order'][$tid][form_clean_id($group)]['#suffix'] = '</div>';
					$form['order'][$tid][form_clean_id($group)]['#weight'] = isset($_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)]) ? $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)]['order'] : $gr_ord;
					$form['order'][$tid][form_clean_id($group)]['order'] = array(
					'#type' => 'hidden',
					'#default_value' => isset($_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)]) ? $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)]['order'] : $gr_ord,
					'#attributes' => array('class' => 'managed-newsletters-group-order'),
					);
					$gr_ord++;


					$ord = 1;
					foreach($nodes as $node)
					{
						if ($_SESSION['managed_newsletters_send']['queue'][$tid][$node->nid] == 'use')
						{
							//TODO: hardcoded images from another module
							if (isset($_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)][$node->nid]['group']))
							{
								$group_val = $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)][$node->nid]['group'];
							}
							else
							{
								$group_val = form_clean_id($group);
							}
							$form['order'][$tid][form_clean_id($group)][$node->nid]['group'] = array(
						'#type' => 'select',
						'#options' => $groups,
						'#title' => $node->title,
						'#default_value' => $group_val,
							//#weight' => $ord,
						'#prefix' => '<div class="managed-newsletters-node-wrapper"><div class="managed-newsletters-node-buttons"><img class="managed-newsletters-node-button-up" src="/profiles/default/modules/gcl_search_saved/images/up.png"><img class="managed-newsletters-node-button-down" src="/profiles/default/modules/gcl_search_saved/images/down.png"></div>' 
						);
						$form['order'][$tid][form_clean_id($group)][$node->nid]['order'] = array(
						'#type' => 'hidden',
						'#default_value' => isset($_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)][$node->nid]['order']) ? $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)][$node->nid]['order'] : $ord,
						'#attributes' => array('class' => 'managed-newsletters-order'),
						//'#weight' => $ord,
						'#suffix' => '</div>'
						);
						$ord++;
						}
					}
				}
			}
		}
		$form['next'] = array(
		'#type' => 'submit',
		'#value' => t('Next'),
		'#weight' => 10
		);
		drupal_add_js(drupal_get_path('module', 'managed_newsletters') . '/managed_newsletters_order_nodes_1.js');
		drupal_add_css(drupal_get_path('module', 'managed_newsletters') . '/managed_newsletters_order_nodes.css');
	}
	else
	{
		drupal_set_message(t('Selected newsletter has not been found'), 'error');
	}
}

function managed_newsletters_send_form_step_edit(&$form, &$form_values)
{
	if ($newsletter = managed_newsletter_template::get_all(null, $_SESSION['managed_newsletters_send']['newsletter']))
	{
		$newsletter->load();
		/*@var $newsletter managed_newsletter_newsletter*/

		$form['edit'] = array(
		'#tree' => true
		);

		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$form['edit'][$tid] = array(
			'#type' => 'fieldset',
			'#title' => $block->get_title(),
			'#tree' => true
				);


				foreach($block->get_nodes() as $group => $nodes)
				{
					$form['edit'][$tid][form_clean_id($group)]['#type'] = 'fieldset';
					$form['edit'][$tid][form_clean_id($group)]['#title'] = $group;
					$form['edit'][$tid][form_clean_id($group)]['#tree'] = true;
					$form['edit'][$tid][form_clean_id($group)]['#weight'] = $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)]['order'];

					$form['edit'][$tid][form_clean_id($group)]['order'] = array(
					'#type' => 'hidden',
					'#default_value' => $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)]['order'],
					);
					$form['edit'][$tid][form_clean_id($group)]['name'] = array(
					'#type' => 'hidden',
					'#default_value' => $group,
					);

					$form['edit'][$tid][form_clean_id($group)]['nodes']['#tree'] = true;
					foreach($nodes as $node)
					{
						if ($_SESSION['managed_newsletters_send']['queue'][$tid][$node->nid] == 'use')
						{
							$new_group = $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)][$node->nid]['group'];
							$weight = $_SESSION['managed_newsletters_send']['order'][$tid][form_clean_id($group)][$node->nid]['order'];
							$form['edit'][$tid][$new_group]['nodes'][$node->nid]['#tree'] = true;
							$form['edit'][$tid][$new_group]['nodes'][$node->nid]['#weight'] = ($weight);
							$form['edit'][$tid][$new_group]['nodes'][$node->nid]['title'] = array(
							'#type' => 'textfield',
								//'#title' => t('Title') . ($weight * 10),
							'#default_value' => isset($_SESSION['managed_newsletters_send']['edit'][$tid][$new_group]['nodes'][$node->nid]['title']) ? $_SESSION['managed_newsletters_send']['edit'][$tid][$new_group]['nodes'][$node->nid]['title'] : $node->title,
							'#attributes' => array('style' => 'width:100%;'),
							'#weight' => ($weight * 10) 
							);
							$form['edit'][$tid][$new_group]['nodes'][$node->nid]['teaser'] = array(
							'#type' => 'textarea',
							//'#title' => t('Teaser') . (($weight * 10) + 1),
							'#default_value' => isset($_SESSION['managed_newsletters_send']['edit'][$tid][$new_group]['nodes'][$node->nid]['teaser']) ? $_SESSION['managed_newsletters_send']['edit'][$tid][$new_group]['nodes'][$node->nid]['teaser'] : $node->teaser,
							'#attributes' => array('style' => 'width:100%;'),
							'#weight' => ($weight * 10) + 1
							);
							$form['edit'][$tid][$new_group]['nodes'][$node->nid]['order'] = array(
							'#type' => 'hidden',
							'#default_value' => $weight,
							);

						}
					}
				}

				foreach($form['edit'][$tid] as $block => $set)
				{
					if (is_array($set) && $set['#type'] == 'fieldset')
					{
						if (count($set['nodes']) == 1)
						{
							unset($form['edit'][$tid][$block]);
						}
						else
						{
							uasort($form['edit'][$tid][$block]['nodes'], "_element_sort");
						}
					}
				}
			}
		}
		$form['next'] = array(
		'#type' => 'submit',
		'#value' => t('Next'),
		'#weight' => 10
		);
		drupal_add_css(drupal_get_path('module', 'managed_newsletters') . '/managed_newsletters_order_nodes.css');
	}
	else
	{
		drupal_set_message(t('Selected newsletter has not been found'), 'error');
	}

}

function managed_newsletters_send_form_step_test(&$form, &$form_values)
{
	global $user;
	$user = user_load(array('uid' => $user->uid));
	if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $_SESSION['managed_newsletters_send']['newsletter']))
	{

		$newsletter->load();
		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$newsletter->get_blocks_templates($tid)->load_changed_nodes($_SESSION['managed_newsletters_send']['edit'][$tid]);
			}
		}
		$form['html'] = array(
		'#type' => 'fieldset',
		'#title' => 'Html'
		);
		$form['html']['html'] = array(
		'#type' => 'markup',
		'#value' => $newsletter->build_html_content($user)
		);

		$form['text'] = array(
		'#type' => 'fieldset',
		'#title' => 'Text'
		);
		$form['text']['text'] = array(
		'#type' => 'markup',
		'#value' => nl2br($newsletter->build_text_content($user))
		);

	}
	$form['desc'] = array(
	'#value' => '<p style="font-weight:bold;">Test newsletter will be sent on your email address</p>'
	);
	$form['send_test'] = array(
	'#type' => 'submit',
	'#value' => t('Send test email'),
	'#weight' => 10
	);
}

function managed_newsletters_send_form_step_send(&$form, &$form_values)
{
	if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $_SESSION['managed_newsletters_send']['newsletter']))
	{
		$newsletter->load();
		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$newsletter->get_blocks_templates($tid)->load_changed_nodes($_SESSION['managed_newsletters_send']['edit'][$tid]);
			}
		}
		if (!isset($_SESSION['managed_newsletters_send']['maillists']))
		{
			$_SESSION['managed_newsletters_send']['maillists'] = $newsletter->get_default_maillists();
		}
		$maillists = array();
		foreach(array_filter($_SESSION['managed_newsletters_send']['maillists']) as $lid)
		{
			$maillist = managed_newsletter_maillist::get_all($lid);
			$maillists[] = $maillist->get_title() . ' ' . l('Preview', 'admin/managed_newsletters/maillists/preview/' . $maillist->get_lid(), array('target' => '_blank'));
		}

		$form['desc'] = array(
		'#value' => '<p style="font-weight:bold;">' . t('@newsletter will be sent to all users from maillists:', array('@newsletter' => $newsletter->get_title())) . '</p>' . theme('item_list', $maillists)
		);

		$form['send'] = array(
		'#type' => 'submit',
		'#value' => t('Send Newsletter'),
		'#weight' => 10
		);
	}
}
function managed_newsletters_send_form_step_reset_queue(&$form, &$form_values)
{
	$form['reset'] = array(
	'#type' => 'submit',
	'#value' => t('Reset Nodes Queue'),
	'#weight' => 10
	);
}

function managed_newsletters_send_test()
{
	global $user;
	$user = user_load(array('uid' => $user->uid));
	if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $_SESSION['managed_newsletters_send']['newsletter']))
	{
		$newsletter->load();
		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$newsletter->get_blocks_templates($tid)->load_changed_nodes($_SESSION['managed_newsletters_send']['edit'][$tid]);
			}
		}
		$sender = managed_newsletter_sender::load_deafult_sender();
		$sender->send_test($newsletter, $user);
		drupal_set_message(t('Test message has been sent to @mail.', array('@mail' => $user->mail)));
	}
	else
	{
		drupal_set_message(t('Test message has not been sent. Newsletter template doesn\'t exist.'), 'error');
	}
}

function managed_newsletters_send()
{

	if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $_SESSION['managed_newsletters_send']['newsletter']))
	{
		$newsletter->load();
		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$newsletter->get_blocks_templates($tid)->load_changed_nodes($_SESSION['managed_newsletters_send']['edit'][$tid]);
			}
		}
		if (!isset($_SESSION['managed_newsletters_send']['maillists']))
		{
			$_SESSION['managed_newsletters_send']['maillists'] = $newsletter->get_default_maillists();
		}
		$maillists = array();
		foreach(array_filter($_SESSION['managed_newsletters_send']['maillists']) as $lid)
		{
			if ($m = managed_newsletter_maillist::get_all($lid))
			{
				$maillists[$lid] = $m;
				$maillists[$lid]->load();
			}
		}
		if (count($maillists) > 0)
		{
			$sender = managed_newsletter_sender::load_deafult_sender();
			$sender->send_newsletter($newsletter, $maillists);
		}
		else
		{
			drupal_set_message(t('Messages have not been sent. No valid maillists were selected.'), 'error');
		}
	}
	else
	{
		drupal_set_message(t('Messages have not been sent. Newsletter template doesn\'t exist.'), 'error');
	}
}

function managed_newsletters_reset_queue()
{
	if ($newsletter = managed_newsletter_template::get_all('managed_newsletter_newsletter', $_SESSION['managed_newsletters_send']['newsletter']))
	{
		$newsletter->load();
		foreach($newsletter->get_blocks_templates() as $tid => $block)
		{
			if (is_a($block, 'managed_newsletter_block_node'))
			{
				$newsletter->get_blocks_templates($tid)->reset_queue($_SESSION['managed_newsletters_send']['queue'][$tid]);
			}
		}
	}
}


function theme_managed_newsletters_send_form(&$form)
{
	if ($form['step']['#value'] == 3 && isset($form['queue']))
	{
		foreach($form['queue'] as $tid => $array1)
		{
			if ($array1['#type'] == 'fieldset')
			{
				$header = array(t('Title'));

				$rows = array();
				foreach($array1 as $nid => $array2)
				{

					if (is_array($array2) && is_numeric($nid))
					{
						$header += $form['queue'][$tid][$nid]['#options'];
						$cells = array();
						$cells[] = $array2['#title'];

						foreach($form['queue'][$tid][$nid] as $key => $el)
						{
							if (isset($header[$key]))
							{
								unset($el['#title']);
								$cells[] = drupal_render($el);
							}
						}
						$rows[] = $cells;
						unset($form['queue'][$tid][$nid]);
					}
				}
				$table = theme('table', $header, $rows);
				$form['queue'][$tid]['table'] = array(
				'#type' => 'markup',
				'#value' => $table
				);
			}
		}
	}
	return drupal_render_form('managed_newsletters_send_form', $form);
}