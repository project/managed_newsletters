if (Drupal.jsEnabled) {
	$(document).ready(function(){
		$('a.managed_newsletter_block_node_link').click(function(){
			$(this).addClass('throbbing');
			$.get(this.href, null, function (data) {
				var result = Drupal.parseJson(data);
				var el = $("a#" + result['id']);
				el.html(result['title']);
				el.attr('href', result['path']);
				$(el).removeClass('throbbing');
			});
			return false;
		});
	});
}