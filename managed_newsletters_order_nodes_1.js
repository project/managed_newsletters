if (Drupal.jsEnabled)
{
	function managed_newsletters_update_order(el, classname)
	{
		var order = 1;
		el.find('.' + classname).each(function(){
			$(this).val(order);
			order++;
		});
	}
	$(document).ready(function(){
		$('.managed-newsletters-node-wrapper LABEL').css('display', 'inline');

		$('.managed-newsletters-node-wrapper SELECT').change(function(){
			var select = $(this);
			var node = select.parents('.managed-newsletters-node-wrapper');
			node.appendTo('#' + select.val());			
			managed_newsletters_update_order(node.parent(), 'managed-newsletters-order');
		});
		
		$('.managed-newsletters-node-wrapper SELECT').each(function(){
			var select = $(this);
			var node = select.parents('.managed-newsletters-node-wrapper');
			node.appendTo('#' + select.val());
		});
		$('.managed-newsletters-group fieldset').each(function(){
			$(this).find('.managed-newsletters-node-wrapper').each(function(){
				var next = true
				var cur = $(this);
				while(next)
				{
					var prev = $(this).prev();
					if (cur.find('.managed-newsletters-order').val() < prev.find('.managed-newsletters-order').val())
					{
						prev.before(cur);
					}
					else
					{
						next = false
					}
				}
			});
			managed_newsletters_update_order($(this), 'managed-newsletters-order');
		});
		
		$('.managed-newsletters-node-button-down').click(function(){
			var el = $(this).parents('.managed-newsletters-node-wrapper');
			var prevel = el.next('.managed-newsletters-node-wrapper');
			prevel.after(el);
			managed_newsletters_update_order(el.parent(), 'managed-newsletters-order');
		});
		$('.managed-newsletters-node-button-up').click(function(){
			var el = $(this).parents('.managed-newsletters-node-wrapper');
			var prevel = el.prev('.managed-newsletters-node-wrapper');
			prevel.before(el);
			managed_newsletters_update_order(el.parent(), 'managed-newsletters-order');
		});
		
		$('.managed-newsletters-group-button-down').click(function(){
			var el = $(this).parents('.managed-newsletters-group');
			var prevel = el.next('.managed-newsletters-group');
			prevel.after(el);
			managed_newsletters_update_order(el.parent(), 'managed-newsletters-group-order');
		});
		$('.managed-newsletters-group-button-up').click(function(){
			var el = $(this).parents('.managed-newsletters-group');
			var prevel = el.prev('.managed-newsletters-group');
			prevel.before(el);
			managed_newsletters_update_order(el.parent(), 'managed-newsletters-group-order');
		});
		
	});
}