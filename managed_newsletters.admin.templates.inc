<?php

/**
 * List of all templates
 * @return string
 */
function managed_newsletters_templates_overview()
{
	$header = array(t('Title'),t('Name'), t('Operations'));
	$rows = array();
	foreach(managed_newsletters_get_all_template_classes() as  $template_class => $title)
	{
		$rows[] = array(array('data' => $title, 'colspan' => 2), array('data' => l(t('Add new'), 'admin/managed_newsletters/templates/add/' . $template_class)));
		foreach(managed_newsletter_template::get_all($template_class) as $tid => $template)
		{
			/*@var $template managed_newsletter_template*/
			$operations = array();
			$operations[] = l(t('Preview'), 'admin/managed_newsletters/templates/preview/' . $template->get_tid());
			$operations[] = l(t('Edit'), 'admin/managed_newsletters/templates/edit/' . $template->get_tid());
			$operations[] = l(t('Delete'), 'admin/managed_newsletters/templates/delete/' . $template->get_tid());
			$cells = array();
			$cells[] = $template->get_title();
			$cells[] = $template->get_name();
			$cells[] = implode(' ', $operations);
			$rows[] = $cells;
		}
	}

	return theme('table', $header, $rows);
}



/**
 * Form for creation or edition of template
 * @param string $class
 * @param int $tid
 * @return string
 */
function managed_newsletters_template_form($class = null, $tid = null)
{
	if ($tid)
	{
		$template = managed_newsletter_template::get_all(null, $tid);
	}
	elseif ($class && class_exists($class))
	{
		$template = new $class();
	}
	if ($template)
	{
		return $template->get_edit_form();
	}
}

/**
 * Saves template in database
 * @param $form_id
 * @param $form_values
 * @return string
 */
function managed_newsletters_template_form_submit($form_id, &$form_values)
{
	$template = $form_values['template'];
	/*@var $template managed_newsletter_template*/
	$template->update($form_values);
	$template->save();
	drupal_set_message('The template has been saved');
	// rebuild administer menu
	cache_clear_all('*', 'cache_menu', TRUE);
	return 'admin/managed_newsletters/templates';
}


/**
 * Shows confimation form for template deletion
 *
 * @param int $tid
 * @return array
 */
function managed_newsletters_template_delete($tid)
{
	$template = managed_newsletter_template::get_all(null, $tid);
	if ($template->get_tid())
	{
		$form['template'] = array('#type' => 'value', '#value' => $template);

		return confirm_form($form,
		t('Are you sure you want to delete template "%name"?', array('%name' => $template->get_title())),
    	'admin/managed_newsletters/templates',
		t('This action cannot be undone.'),
		t('Delete'), t('Cancel'));
	}
	else
	{
		drupal_not_found();
		exit;
	}
}

/**
 * Removes template
 *
 * @param string $form_id
 * @param array $form_values
 * @return string
 */
function managed_newsletters_template_delete_submit($form_id, $form_values)
{
	$template = $form_values['template'];
	$message = t('Template "%name" has been removed.', array('%name' => $template->get_title()));
	watchdog('managed_newsletters', $message);
	drupal_set_message($message);
	$template->delete();
	// rebuild administer menu
	cache_clear_all('*', 'cache_menu', TRUE);
	return 'admin/managed_newsletters/templates';
}

function managed_newsletters_preview($tid, $account = null)
{
	if ($account == null)
	{
		global $user;
		$account = user_load(array('uid' => $user->uid));
	}
	else
	{
		$account = user_load(array('uid' => $account));
	}

	$output = '';
	if ($newsletter = managed_newsletter_template::get_all(null, $tid))
	{
		$newsletter->load();
		$form = array();
		
		$form['html'] = array(
		'#type' => 'fieldset',
		'#title' => 'Html'
		);
		$form['html']['html'] = array(
		'#type' => 'markup',
		'#value' => $newsletter->build_html_content($account)
		);
		
		$form['text'] = array(
		'#type' => 'fieldset',
		'#title' => 'Text'
		);
		$form['text']['text'] = array(
		'#type' => 'markup',
		'#value' => nl2br($newsletter->build_text_content($account))
		);
		$output = drupal_render($form);
	}
	return $output;
}