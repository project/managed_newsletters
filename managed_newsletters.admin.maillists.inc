<?php

/**
 * List of all maillists
 * @return string
 */
function managed_newsletters_maillists_overview()
{
	$header = array(t('Title'), t('Operations'));
	$rows = array();
	foreach(managed_newsletters_get_all_maillist_classes() as $class => $title)
	{
		$rows[] = array(array('data' => $title), array('data' => l(t('Add new'), 'admin/managed_newsletters/maillists/add/' . $class)));
		foreach(managed_newsletter_maillist::get_all(null, $class) as $id => $maillist)
		{
			/*@var $maillist managed_newsletter_maillists*/
			$operations = array();
			$operations[] = l(t('Preview'), 'admin/managed_newsletters/maillists/preview/' . $maillist->get_lid());
			$operations[] = l(t('Edit'), 'admin/managed_newsletters/maillists/edit/' . $maillist->get_lid());
			$operations[] = l(t('Delete'), 'admin/managed_newsletters/maillists/delete/' . $maillist->get_lid());
			$cells = array();
			$cells[] = $maillist->get_title();
			$cells[] = implode(' ', $operations);
			$rows[] = $cells;
		}
	}

	return theme('table', $header, $rows);
}



/**
 * Form for creation or edition of template
 * @param string $class
 * @param int $tid
 * @return string
 */
function managed_newsletters_maillist_form($class = null, $lid = null)
{
	if ($lid)
	{
		$maillist = managed_newsletter_maillist::get_all($lid);
	}
	elseif ($class && class_exists($class))
	{
		$maillist = new $class();
	}
	if ($maillist)
	{
		return $maillist->get_edit_form();
	}
}

/**
 * Saves template in database
 * @param $form_id
 * @param $form_values
 * @return string
 */
function managed_newsletters_maillist_form_submit($form_id, &$form_values)
{
	$maillist = $form_values['maillist'];
	/*@var $maillist managed_newsletter_maillist*/
	$maillist->update($form_values);
	$maillist->save();
	drupal_set_message('The maillist has been saved');
	// rebuild administer menu
	cache_clear_all('*', 'cache_menu', TRUE);
	return 'admin/managed_newsletters/maillists';
}


/**
 * Shows confimation form for template deletion
 *
 * @param int $tid
 * @return array
 */
function managed_newsletters_maillist_delete($lid)
{
	$maillist = managed_newsletter_maillist::get_all($lid);
	if ($maillist->get_lid())
	{
		$form['maillist'] = array('#type' => 'value', '#value' => $maillist);

		return confirm_form($form,
		t('Are you sure you want to delete maillist "%name"?', array('%name' => $maillist->get_title())),
    	'admin/managed_newsletters/maillists',
		t('This action cannot be undone.'),
		t('Delete'), t('Cancel'));
	}
	else
	{
		drupal_not_found();
		exit;
	}
}

/**
 * Removes template
 *
 * @param string $form_id
 * @param array $form_values
 * @return string
 */
function managed_newsletters_maillist_delete_submit($form_id, $form_values)
{
	$maillist = $form_values['maillist'];
	$message = t('Maillist "%name" has been removed.', array('%name' => $maillist->get_title()));
	watchdog('managed_newsletters', $message);
	drupal_set_message($message);
	$maillist->delete();
	// rebuild administer menu
	cache_clear_all('*', 'cache_menu', TRUE);
	return 'admin/managed_newsletters/maillists';
}

function managed_newsletters_maillist_preview($lid)
{
	$maillist = managed_newsletter_maillist::get_all($lid);
	if ($maillist->get_lid())
	{
		$output = '';
		$rows = array();
		foreach($maillist->get_recipients() as $email => $uid)
		{
			$rows[] = array($uid, $email);
		}
		
		$header = array('uid', 'mail');
		$view = views_get_view($maillist->get_view());
		if ($view->url)
		{
			$output .= '<p>' . l('Open view', $view->url, array('target' => '_blank')) . '</p>';
		}
		$output .= theme('table', $header, $rows);
		return $output;
	}
	else
	{
		drupal_not_found();
		exit;
	}
}