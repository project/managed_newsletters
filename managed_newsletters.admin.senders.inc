<?php

function managed_newsletters_senders_form()
{
	$senders = managed_newsletters_get_all_senders_classes();
	$form = array();
	$form['default_sender'] = array(
	'#type' => 'radios',
	'#title' => t('Default send method'),
	'#options' => $senders,
	'#default_value' => managed_newsletter_sender::get_deafult_sender(), 
	'#required' => true
	);
	$form['submit'] = array(
	'#type' => 'submit',
	'#value' => t('Save')
	);
	
	return $form;
}

function managed_newsletters_senders_form_submit($form_id, &$form_values)
{
	managed_newsletter_sender::set_deafult_sender($form_values['default_sender']);
}