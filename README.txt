
Managed Newsletters

Allows to send highly managable newsletter from the site.

To install, place the entire module folder into your modules directory.
Go to Administer -> Site Building -> Modules and enable the Managed Newsletters module.

This is a complex module that require quite a bit of building and setting up. There is *no* out of the box experience here.

Some rudimentary instructions (I will make something pretty and easier to follow shortly)

The use-case for this module is sending daily or weekly updates of new content to a group of users. User groups are built with a specific view, allowing you to use modular opt-outs. The workflow of this module is completely built around this use-case, and allows you to send out newsletters in a matter of minutes. 

Installing:
Install this module in the usual fashion.

Building:
Step one is to define an email template. We built this system to be able to send complex message bodies - for example, HTML with multiple columns, and we strongly suggest you first build the email template, and test it using the excellent campaignmonitor testing service first. Once you have a template you are happy with, you can start to create template regions in the newsletter system, for example headers, footers, content sections etc. A content section can contain another content section.

Inside the template sections you create both HTML as well as plain text templates, using content, and optionally, login tokens. 

Using:
As you create content on your site, nodes will be added to a queue. when you hit "create newsletter", you are presented with a list of content added since your last queue reset. 

- For each node you wish to use, select "use". 
- For each node you might want to use in the near future, select "hold".
- For nodes you dont want to use in this newsletter, select "discard"

click next, and arrange your newsletter order. 
click next for previews
click next to preview the adress group (this _must_ be done, due to some caching issue) - if the list is empty, clear the views cache using the link provided. 
click send test.
send the newsletter is all is ok.

shout if any questions - I promise more detailed instructions soon. 

